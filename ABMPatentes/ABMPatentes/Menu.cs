﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ABMPatentes
{
    public class Menu  : Abm
    {
        string opcionMenu = "";
        public void iniciar()
        {
            do
            {
                cabecera();
            } while (opcionMenu != "0");
        }

        private void cabecera()
        {
            Console.Write("------Control de Patentes------");
            Console.Write("\n");
            Console.Write("------    Menu principal ------");
            Console.Write("\n");
            Console.Write("1. [Crear Patente]       | 3. [Modificar Patente]");
            Console.Write("\n");
            Console.Write("2. [Eliminar Patente ]   | 4. [Mostrar patente]");
            Console.Write("\n");
            Console.Write("5. [Salir]");
            Console.Write("\n");
            Console.Write("Seleccione una opción: ");
            opcionMenu = Console.ReadLine();
            seleccionMenu(opcionMenu);
        }

        private void seleccionMenu(string op)
        {
            if (op == "")
                return;
            switch (op)
            {
                case "1":
                    Console.Clear();
                    crearPatente();
                    retornarMenu();
                    Console.ReadKey();
                    break;
                case "2":
                    Console.Clear();
                    Eliminar();
                    retornarMenu();
                    Console.ReadKey();
                    break;

                case "3":
                    Console.Clear();
                    Modificar();
                    retornarMenu();
                    Console.ReadKey();
                    break;
                case "4":
                    Console.Clear();
                    lista();
                    retornarMenu();
                    Console.ReadKey();
                    break;
                case "5":
                    Console.Clear();
                    cabecera();
                    Console.ReadKey();
                    break;
                default:
                    Console.WriteLine("Seleccion invalida");
                    break;
            }
        }
        private void retornarMenu()
        {
            string op;//opcion
            Console.WriteLine("presione 5 para retornar al menu ppal");
            op = Console.ReadLine();
            seleccionMenu(op);
        }
    }
}
