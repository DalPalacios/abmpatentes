﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace ABMPatentes
{
    public class Abm
    {
        List<Patente> patentes = new List<Patente>();
        int idPatente;
        string numPatente;

        public void crearPatente()
        {
            Console.WriteLine("Crear Patente");
            Console.WriteLine("\n");


            Console.WriteLine("Ingrese patente: ");
            numPatente = Console.ReadLine();

            bool validar = validarPatente(numPatente);
            if (validar)
            {
                if (!existePatente(numPatente))
                {
                    patentes.Add(new Patente(patentes.Count + 1, numPatente));
                    Console.Write("\n");
                    Console.WriteLine("Patente guardada");
                    Console.Write("\n");
                }
                else
                {
                    Console.WriteLine("Patente repetida ");
                }
            }
            else
            {
                Console.WriteLine("Patente inválida, no contiene 3 numeros y 3 letras, ejemplo: 'aaa111'");
            }

        }
        private bool existePatente(String p)
        {
            string buscarP = p;
            foreach (var item in patentes)
            {
                if (buscarP == item.patente)
                {
                    return true;

                }

            }
            return false;


        }
        private bool listaVacia()
        {
            if (patentes.Count == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool validarPatente(string patente)
        {
            if (!Regex.IsMatch(patente, "^[a-z-A-Z]{3}[0-9]{3}$"))
            {
                Console.WriteLine("Error en Patente", "Por favor ingrese patente que sean de 3 letras y 3 números");
                return false;
            }
            return true;
        }
        public void buscar()
        {
            if (listaVacia() == true)
            {
                Console.WriteLine("no se encuentran datos");
            }
            else
            {
                string buscar;
                Console.Write("ingrese patente para buscar: ");
                buscar = Console.ReadLine();
                foreach (Patente item in patentes)
                {
                    if (buscar == item.patente)
                    {
                        Console.WriteLine(" id: {0} | patente: {1} ", item.id, item.patente);

                    }
                    else
                    {
                        Console.WriteLine("no se encontro ninguna patente");
                    }
                }
            }
        }
        public void Eliminar()
        {
            string buscar;
            if (listaVacia())
            {
                Console.WriteLine("No se encuentra ninguna patente en la lista");
            }
            else
            {
                Console.Write("Ingrese patente: ");
                buscar = Console.ReadLine();
                foreach (var item in patentes)
                {
                    if (buscar == item.patente)
                    {
                        Console.WriteLine("---------------------------");
                        Console.WriteLine(" id: {0} | patente: {1} ", item.id, item.patente);
                        patentes.Remove(item);
                        Console.WriteLine("\n La patente ha sido eliminado");
                        break;
                    }
                    else
                    {
                        Console.WriteLine("no se encuentra la patente ingresada");
                    }
                }
            }
        }
        public void Modificar()
        {
            if (listaVacia())
            {
                Console.WriteLine("No se encuentra ninguna patente en la lista");
            }
            else
            {
                Patente patente = new Patente();
                string buscar;
                Console.Write(" Ingrese una patente a modificar: ");
                buscar = Console.ReadLine();
                try
                {
                    foreach (Patente item in patentes)
                    {
                        if (buscar == item.patente)
                        {

                            Console.WriteLine(" id: {0} | patente: {1} ", item.id, item.patente);
                            Console.WriteLine("---------------------------\n\n");
                            Console.Write("Ingrese patente: ");
                            patente.patente = Console.ReadLine();

                            bool validar = validarPatente(patente.patente);
                            if (validar)
                            {
                                if (!existePatente(patente.patente))
                                {
                                    item.patente = patente.patente;
                                    Console.WriteLine("\n");
                                    Console.WriteLine("su patente han sido modificada");
                                }
                                else
                                {
                                    Console.WriteLine("Patente inválida, no contiene 3 numeros y 3 letras, ejemplo: 'aaa111'");
                                    break;

                                }

                            }
                        }
                    }
                }
                catch (Exception)
                {

                    throw;
                }


            }
        }
        public void lista()
        {
            if (listaVacia() == true)
            {
                Console.WriteLine("no se encuenta ninguna patente en la lista");

            }
            else
            {
                Console.WriteLine("Total de patentes: " + patentes.Count);
                Console.WriteLine("-----Lista-----");
                foreach (Patente item in patentes)
                {
                    imprimir(item);
                }
            }
        }

        public void imprimir(Patente patente)
        {
            Console.WriteLine("---------------------------");
            Console.WriteLine(" id: {0} | patente: {1}  ", patente.id, patente.patente);


        }

    }
}

